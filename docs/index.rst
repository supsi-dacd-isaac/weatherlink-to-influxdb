.. Weatherlink to InfluxDB documentation master file, created by
   sphinx-quickstart on Tue Nov 20 10:48:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Weatherlink to InfluxDB's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
